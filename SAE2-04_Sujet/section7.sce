V=[14,25,22,40,31,31,35,46,40,38,34,26];
E=[109,180,158,276,225,209,230,342,293,234,244,176]

scatter(V,E)

moyV=mean(V)
moyE=mean(E)

VarV=mean((V-moyV).*(V-moyV))
disp(VarV)
VarE=mean((E-moyE).*(E-moyE))
disp(VarE)

CovVE=mean((V-moyV).*(E-moyE))
disp(CovVE)

pVE=CovVE/sqrt(VarV.*VarE)
disp(pVE)

a=CovVE/VarV
b=moyE-a.*moyV
x=[min(V):0.02:max(V)];
y=a*x+b
//propriété de la droite


plot2d(x,y)
